from setuptools import setup, find_packages
from os.path import join, dirname
import tacker

setup(
    name='Tacker',
    version=tacker.__version__,
    packages=find_packages(),
    long_description=open(join(dirname(__file__), 'README.md')).read(),
    entry_points={
        'console_scripts':
        ['tacker = tacker.tracker:main']
    }
)