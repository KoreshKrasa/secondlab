import argparse
import logging
import os
import signal
import sys
import time
from collections import OrderedDict
from configparser import ConfigParser
from datetime import datetime

import tacker.terminal as terminal
from tacker.parse import TackerParser
from tacker.core.system import System
from tacker.core.task import Delta, Plan, Status, Task
from tacker.core.user import User
from tacker.tdaemon import TackerDaemon

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(message)s',
                    filename='logging.log', filemode='a')


class Tacker:
    '''Main class of the tracker'''

    def __init__(self, config_path=None):
        logging.info('| Tacker start work')
        config = ConfigParser()
        self.error = False

        if config_path is None:
            config_path = 'config.ini'

        # load/cread config file
        if not os.path.exists(config_path):
            config['TACKER'] = {'data_path': 'data',
                                'data_file_name': 'tackerdata.json',
                                'data_file_main': 'm',
                                'root_list_name': 'root'}
            with open(config_path, 'w') as config_file:
                config.write(config_file)
        else:
            config.read(config_path)

        try:
            self.d_name = config['TACKER']['data_file_name']
            self.m_name = config['TACKER']['data_file_main'] + self.d_name
            self.path = config['TACKER']['data_path']
            self.root_list_name = config['TACKER']['root_list_name']
        except KeyError:
            raise KeyError('Delete config file to fix it')

        # load/create system data
        if not os.path.exists(self.path):
            os.mkdir(self.path)
        path = os.path.join(self.path, self.d_name)
        if os.path.exists(path):
            self.system = System._load_system_data(path)
        else:
            self.system = System()
            system_file = open(path, 'w')
            system_file.close()

        # initialize parser dict
        self.parser_map = OrderedDict([
            (TackerParser.user_parser_delete_dest, self.delete_user),
            (TackerParser.user_parser_show_dest, self.show_all_users),
            (TackerParser.list_parser_choose_dest, self.choose_list),
            (TackerParser.list_parser_create_dest, self.create_list),
            (TackerParser.list_parser_name_dest, self.rename_list),
            (TackerParser.list_parser_move_task_dest, self.move_task_to_list),
            (TackerParser.list_parser_delete_task_dest, self.delete_task_from_list),
            (TackerParser.list_parser_move_dest, self.move_list),
            (TackerParser.list_parser_show_dest, self.show_list),
            (TackerParser.list_parser_all_dest, self.show_all_list),
            (TackerParser.list_parser_delete_dest, self.delete_list),
            (TackerParser.task_parser_choose_dest, self.choose_task),
            (TackerParser.task_parser_create_dest, self.add_task),
            (TackerParser.task_parser_priority_dest, self.set_priority),
            (TackerParser.task_parser_time_dest, self.edit_expire_date),
            (TackerParser.task_parser_name_dest, self.edit_task_name),
            (TackerParser.task_parser_description_dest, self.edit_task_description),
            (TackerParser.task_parser_sub_dest, self.add_subtask),
            (TackerParser.task_parser_link_dest, self.add_link),
            (TackerParser.task_parser_reset_dest, self.reset_task),
            (TackerParser.task_parser_complete_dest, self.complete_task),
            (TackerParser.task_parser_show_dest, self.show_task),
            (TackerParser.task_parser_all_dest, self.show_all_tasks),
            (TackerParser.task_parser_archive_dest, self.show_archive),
            (TackerParser.task_parser_plan_dest, self.set_plan),
            (TackerParser.task_parser_plan_delete_dest, self.delete_plan),
            (TackerParser.task_parser_share_dest, self.share_task),
            (TackerParser.task_parser_delete_dest, self.delete_task)])

    def save_data(self):
        '''Save system data'''

        logging.info('| Tacker end work\n')
        path = os.path.join(self.path, self.m_name)
        self.system._save_system_data(path)

    def print_info(self):
        '''Print current user, current task and current list info'''

        print('User:')
        self.show_user(tab=1)
        print('\nTask:')
        self.show_task(tab=1)
        print('\nList:')
        self.show_list(tab=1)

    # User segment

    # choose current user or create new one
    def choose_user(self, user_name):
        '''Set current user if user name isn't exist create new one

        :param user_name: Name of the user
        :type user_name: Must be str
        :raise TypeError: If user_name is not str
        '''

        if type(user_name) is not str:
            raise TypeError('user_name must be str')
        if user_name in self.system.all_users:
            logging.info('| Tacker choose user %s', user_name)
            self.system.current_user = self.system.all_users[user_name]
        else:
            logging.info('| Tacker create new user %s', user_name)
            new_user = User(self.system.users_max_index, user_name)
            self.system.users_max_index += 1
            self.system.all_users[user_name] = new_user
            self.system.current_user = new_user
        self.system.current_task = None
        self.system.current_list = None

    def show_user(self, tab=None):
        '''Show current user information

        :param tab: Set indent
        :type tab: Must be int or None
        '''

        logging.info('| Tacker show user %s', self.system.current_user.name)
        if tab is None:
            tab = 0
        print('{}Name: {}'.format('  '*tab, self.system.current_user.name))

    def show_all_users(self):
        '''Show all users names'''

        for user in self.system.all_users:
            print(user)

    def delete_user(self):
        '''Delete current user and all it information
        also set current user to None
        '''

        logging.info('| Tacker delete user %s', self.system.current_user.name)
        cr_user = self.system.current_user
        all_tasks = self.system.all_tasks
        for index in cr_user.lists.all_tasks:
            all_tasks[str(index)].users.remove(cr_user.name)
            if len(all_tasks[str(index)].users) == 0:
                if self.system.current_task is all_tasks[str(index)]:
                    self.system.current_task = None
                del all_tasks[str(index)]
        for plan in self.system.plans:
            if cr_user.name in plan.users:
                plan.users.remove(cr_user.name)
            if len(plan.users) == 0:
                self.system.plans.remove(plan)
        if cr_user.name in self.system.all_users:
            del self.system.all_users[cr_user.name]
            self.system.current_user = None

    # list segment

    def show_list(self, tlist=None, tab=None):
        '''Show current list information

        :param tab: Set indent
        :type tab: Must be int or None
        '''
        if tlist is None:
            tlist = self.system.current_list
        if tlist is not None:
            logging.info('| Tacker show list %s', tlist.name)
            print('{}+ {} | {}:'.format('  '*tab, tlist.name, tlist.creation_date))
            tasks = [self.system.all_tasks[str(index)]
                     for index in tlist.tasks]
            tasks.sort(key=lambda t: t.priority, reverse=True)
            for task in tasks:
                print('{}* {} | {}'.format('  '*(tab+1),
                                           task.name, task.creation_date))
            for child_ls in tlist.childs:
                self.show_list(child_ls, tab+1)

    def create_list(self, name):
        '''Add new list to current user

        :param name: Name of the list
        :type name: Must be str 
        :raise TypeError: If name is not str
        :raise ValueError: If name == root list name(root by default)
        '''

        if type(name) is not str:
            raise TypeError('name must be str')
        if name == self.root_list_name:
            raise ValueError('inappropriate name')
        logging.info('| Tacker create list %s', name)
        new_list = self.system.current_user.create_list(name)
        self.system.current_list = new_list

    def rename_list(self, name):
        '''Rename current list

        :param name: New name of the list
        :type name: Must be str
        :raise TypeError: If name is not str
        :raise ValueError: If list is not choosen or name is root 
        '''

        if type(name) is not str:
            raise TypeError('name must be str')

        if name == self.root_list_name:
            raise ValueError('inappropriate name')

        if self.system.current_list is None:
            raise ValueError('current list is not choosen')

        logging.info('| Tacker rename list %s to %s',
                     self.system.current_list.name, name)
        self.system.current_list.name = name

    def move_list(self, name):
        '''Move current list to new directory

        :param name: Name of the new parant directory
        :type name: Must be str
        :raise TypeError: If name is not str
        :raise ValueError: If list is not choosen
        '''

        if type(name) is not str:
            raise TypeError('name must be str')

        if self.system.current_list is None:
            raise ValueError('current list is not choosen')

        if name != self.root_list_name:
            lists = self.system.current_user.lists.find_list(name)
            if self.system.current_list in lists:
                lists.remove(self.system.current_list)
            tlist = terminal._choices_print(lists)
            if tlist is not None:
                self.system.current_user.move_list_to_list(
                    self.system.current_list, tlist)
        if name == self.root_list_name:
            self.system.current_user.move_list_to_list(
                self.system.current_list)

        logging.info('| Tacker move list %s to %s',
                     self.system.current_list.name, name)

    def delete_task_from_list(self, task_name):
        '''Delete task from current list

        :param task_name: Name of the task to be deleted
        :type task_name: Must be str
        :raise TypeError: If name is not str
        :raise ValueError: If list is not choosen
        '''
        if type(task_name) is not str:
            raise TypeError('task_name must be str')

        if self.system.current_list is None:
            raise ValueError('current list is not choosen')

        if task_name is not None:
            tasks = []
            for index in self.system.current_list.tasks:
                if task_name == self.system.all_tasks[str(index)].name:
                    tasks.append(self.system.all_tasks[str(index)])
            task = terminal._choices_print(tasks)
            if task is not None:
                self.system.current_list.tasks.remove(task.index)
                logging.info('| Tacker delete task %s from list %s',
                             task_name, self.system.current_list.name)

    def move_task_to_list(self, task_name):
        '''Move task to current list

        :param task_name: Name of the task to be moved
        :type task_name: Must be str
        :raise TypeError: If name is not str
        :raise ValueError: If list is not choosen
        '''

        if type(task_name) is not str:
            raise TypeError('task_name must be str')
        if self.system.current_list is None:
            raise ValueError('current list is not choosen')
        tasks = []
        for index in self.system.current_user.lists.all_tasks:
            task = self.system.all_tasks[str(index)]
            if task_name == task.name:
                if task.index not in self.system.current_list.tasks:
                    tasks.append(self.system.all_tasks[str(index)])
        task = terminal._choices_print(tasks)
        if task is not None:
            self.system.current_list.tasks.append(task.index)
            logging.info('| Tacker move task %s to list %s',
                         task_name, self.system.current_list.name)

    def delete_list(self):
        '''Delete current list and all sublists

        :raise ValueError: If list is not choosen
        '''

        if self.system.current_list is None:
            raise ValueError('current list is not choosen')
        self.system.current_user.delete_list(self.system.current_list)
        logging.info('| Tacker delete list %s', self.system.current_list.name)
        self.system.current_list = None

    def show_all_list(self):
        '''Show all lists of the current user'''

        print('User: {}'.format(self.system.current_user.name))
        for tlist in self.system.current_user.lists.user_lists:
            print('{} | {}'.format(tlist.name, tlist.creation_date))
        logging.info('| Tacker show all lists')

    def choose_list(self, name):
        '''Set current list

        :param name: Name of the list to be current
        :type name: Must be str or None(If None set to Root)
        :raise TypeError: If name is not str
        '''

        if type(name) is not str:
            raise TypeError('name must be str')
        if name != self.root_list_name:
            lists = self.system.current_user.lists.find_list(name)
            if self.system.current_list in lists:
                lists.remove(self.system.current_list)
            tlist = terminal._choices_print(lists)
            if tlist is not None:
                self.system.current_list = tlist
        else:
            self.system.current_list = None
        logging.info('| Tacker choose list')

    # Task segment

    def share_task(self, user_name, task=None):
        '''Share current task with user

        :param user_name: Name of the user with whom you want share task
        :param task: Task to be shared
        :type user_name: Must be str
        :type task: Can be task or None
        :raise TypeError: If user_name is not str
        :raise ValueError: If task is not choosen
        '''

        if task is None:
            task = self.system.current_task

        if type(user_name) is not str:
            raise TypeError('user_name must be str')

        if task is None:
            raise ValueError('current task is not choosen')

        task = self.system.current_task
        if user_name in self.system.all_users:
            sh_user = self.system.all_users[user_name]
            task.users.append(user_name)
            sh_user.lists.all_tasks.append(task.index)
            for sub in task.subtasks:
                self.share_task(user_name, sub)
            for linked in task.linked:
                self.share_task(user_name, linked)
        logging.info('| Tacker share task %s to %s', task.name, user_name)

    def show_archive(self):
        '''Show user completed tasks'''

        for index in self.system.current_user.lists.bin:
            task = self.system.all_tasks[str(index)]
            print('{} | Complete date: {}'.format(
                task.name, task.complete_date))
        logging.info('| Tacker show archive')

    def reset_task(self):
        '''Set task back to pending

        :raise ValueError: If task is not choosen
        '''

        if self.system.current_task is None:
            raise ValueError('current task is not choosen')

        cr_task = self.system.current_task
        cr_task.reset()
        cr_user = self.system.current_user
        if cr_task.index in cr_user.lists.bin:
            cr_user.lists.bin.remove(cr_task.index)
        logging.info('| Tacker reset task %s', cr_task.name)

    def show_task(self, tab=None):
        '''Show current task information

        :param tab: Param to add indent
        :type tab: Can be int or None
        '''

        if tab is None:
            tab = 0
        if self.system.current_task is not None:
            task_to_show = self.system.current_task
            print('{}Name: {}'.format('  '*tab, task_to_show.name))
            print('{}Creation date: {}'.format(
                '  '*tab, task_to_show.creation_date))
            print('{}Status: {}'.format('  '*tab, task_to_show.status))
            if task_to_show.expire_date is not None:
                print('{}Expire date: {}'.format(
                    '  '*tab, task_to_show.expire_date))
            if task_to_show.description is not None:
                print('{}Description: {}'.format(
                    '  '*tab, task_to_show.description))
            if task_to_show.linked is not None:
                print('{}Linked:'.format('  '*tab))
                linked = task_to_show.linked
                linked.sort(key=lambda t: t.priority, reverse=True)
                for link in linked:
                    print('{}  * {} | {}'.format('  '*tab,
                                                 link.name, link.creation_date))
            if task_to_show.subtasks is not None:
                print('{}Subtasks:'.format('  '*tab))
                sub = task_to_show.subtasks
                sub.sort(key=lambda t: t.priority, reverse=True)
                for sub_task in sub:
                    print('{}  * {} | {}'.format('  '*tab,
                                                 sub_task.name, sub_task.creation_date))
            logging.info('| Tacker show task %s', self.system.current_task)

    def edit_task_name(self, name):
        '''Set task name

        :param name: New name of the task
        :type name: Must be str
        :raise TypeError: If name is not str
        :raise ValueError: If task is not choosen
        '''

        if type(name) is not str:
            raise TypeError('name must be str')

        if self.system.current_task is None:
            raise ValueError('task is not choosen')

        logging.info('| Tacker rename task %s to %s',
                     self.system.current_task.name, name)
        self.system.current_task.name = name

    def edit_task_description(self, description):
        '''Set task description

        :param description: New description of the task
        :type description: Must be str
        :raise TypeError: If description is not str
        :raise ValueError: If task is not choosen
        '''

        if type(description) is not str:
            raise TypeError('description must be str')

        if self.system.current_task is None:
            raise ValueError('task is not choosen')

        self.system.current_task.description = description
        logging.info(
            '| Tacker edit task description')

    def add_task(self, name):
        '''Add new task to current user

        :param name: Name of the new task
        :param type: Must be str
        :raise TypeError: If name is not str
        '''

        if type(name) is not str:
            raise TypeError('name must be str')

        new_task = Task(self.system.tasks_max_index, name)
        new_task.users.append(self.system.current_user.name)
        self.system.tasks_max_index += 1
        self.system.current_user.add_task(new_task.index)
        self.system.all_tasks[str(new_task.index)] = new_task
        self.system.current_task = new_task
        if self.system.current_list is not None:
            self.system.current_user.move_task_to_list(
                new_task.index, self.system.current_list)
        logging.info('| Tacker add task %s', name)

    def add_subtask(self, name):
        '''Add subtask to current task

        :param name: Name of the subtask
        :param type: Must be str
        :raise TypeError: If description is not str
        :raise ValueError: If task is not choosen
        '''

        if type(name) is not str:
            raise TypeError('name must be str')

        if self.system.current_task is None:
            raise ValueError('current task is not choosen')

        new_task = Task(self.system.tasks_max_index, name)
        new_task.users.append(self.system.current_user.name)
        new_task.parent = self.system.current_task
        self.system.tasks_max_index += 1
        self.system.current_user.add_task(new_task.index)
        self.system.all_tasks[str(new_task.index)] = new_task
        self.system.current_task.subtasks.append(new_task)
        logging.info('| Tacker add subtask %s', name)

    def set_plan(self, delta):
        '''Create plan using current task as example

        :param delta: String that contains informations 
        about time interval
        :type delta: Must be str that contains NumberLeter combinations
        to set delta time
        :raise TypeError: If delta is not str
        :raise ValueError: If task is not choosen
        :raise ValueError: If expire_date is not set
        :raise ValueError: If time interval is not set
        '''

        if type(delta) is not str:
            raise TypeError('delta must be str')

        if self.system.current_task is None:
            raise ValueError('current task is not choosen')

        if self.system.current_task.expire_date is None:
            raise ValueError("expire date isn't set")

        parsed_delta = terminal._parse_plan_delta(delta)
        if all(val == 0 for val in parsed_delta):
            raise ValueError("time interval isn't set")

        dt = Delta(*parsed_delta)
        new_plan = Plan(dt, self.system.current_task)
        new_plan.add_user(self.system.current_user.name)
        self.system.plans.append(new_plan)
        logging.info('| Tacker create plan')

    def delete_plan(self):
        '''Delete plan'''

        plans = self.system.plans
        plan_to_delete = terminal._choices_print(plans)
        if plan_to_delete is not None:
            self.system.plans.remove(plan_to_delete)
            logging.info('| Tacker delete plan')

    def edit_expire_date(self, date):
        '''Edit task expire date

        :param date: After this date task become failed
        :type date: Str 'DD MM YYYY HH:MM'
        :raise ValueError: If date is not str
        :raise ValueError: If date has wrong format
        '''

        if type(date) is not str:
            raise TypeError('date must be str')

        try:
            expire_date = datetime.strptime(date, '%d %m %Y %H:%M')
            self.system.current_task.expire_date = expire_date
            logging.info('| Tacker edite task %s expire date',
                         self.system.current_task.name)
        except ValueError:
            raise ValueError('Wrong date format')

    def choose_task(self, name):
        '''Set current task to task with specific name

        :param name: Task with this name will become current
        :type name: Must be str
        :raise ValueError: If name is not str
        '''

        if type(name) is not str:
            raise ValueError('name must be str')

        tasks = []
        for index in self.system.current_user.lists.all_tasks:
            if name == self.system.all_tasks[str(index)].name:
                tasks.append(self.system.all_tasks[str(index)])
        task = terminal._choices_print(tasks)
        if task is not None:
            self.system.current_task = task
            logging.info('| Tacker choose task %s', task.name)

    def add_link(self, name):
        '''Add linked task to current

        :param name: Name of task to be linked
        :type name: Must be str
        :raise ValueError: If task is not choosen
        :raise ValueError: If name is not str
        '''

        if type(name) is not str:
            raise ValueError('name must be str')

        if self.system.current_task is None:
            raise ValueError('task is not choosen')

        tasks = []
        for index in self.system.current_user.lists.all_tasks:
            if name == self.system.all_tasks[str(index)].name:
                tasks.append(self.system.all_tasks[str(index)])
        task = terminal._choices_print(tasks)
        if task is not None:
            self.system.current_task.linked.append(task)
            task.linked_parent.append(self.system.current_task)
            logging.info('| Tacker add link to task %s',
                         self.system.current_task.name)

    def complete_task(self):
        '''Set current task to complete

        :raise ValueError: If task is not choosen
        '''

        if self.system.current_task is None:
            raise ValueError("current task isn't choosen")

        self.system.current_task.complete_task()
        indexes = self.system.current_task._get_sub_indexes()
        for index in indexes:
            if index not in self.system.current_user.lists.bin:
                self.system.current_user.lists.bin.append(index)
                logging.info('| Tacker complete task %s',
                             self.system.current_task.name)

    def set_priority(self, priority):
        '''Set priority for current task

        :param priority: Str that contains + and - to set task priority
        :type priority: Must be str
        :raise ValueError: If priority is not str
        :raise ValueError: If task is not choosen
        '''

        if type(priority) is not str:
            raise ValueError('priority must be str')

        if self.system.current_task is None:
            raise ValueError("current task isn't choosen")

        self.system.current_task.priority = terminal._parse_priority(
            priority)
        logging.info('| Tacker edit task priority')

    def delete_task(self, task=None):
        '''Delete current task

        :raise ValueError: If task is not choosen
        '''

        if task is None:
            task = self.system.current_task
            self.system.current_task = None

        if task is None:
            raise ValueError('current task is not choosen')

        if task is not None:
            for user_name in task.users:
                user = self.system.all_users[user_name]
                if task.index in user.lists.all_tasks:
                    user.lists.all_tasks.remove(task.index)
                if task.index in user.lists.bin:
                    user.lists.bin.remove(task.index)
                for tlist in user.lists.user_lists:
                    if task.index in tlist.tasks:
                        tlist.tasks.remove(task.index)
            for link in task.linked_parent:
                link.linked.remove(task)
            for sub in task.subtasks:
                self.delete_task(sub)
            if task.parent is not None:
                task.parent.subtasks.remove(task)
            del self.system.all_tasks[str(task.index)]
            logging.info('| Tacker delete task %s', task.name)

    def show_all_tasks(self):
        '''Show all tasks of a user'''

        cr_user = self.system.current_user
        all_tasks = self.system.all_tasks
        tasks = [all_tasks[str(index)] for index in cr_user.lists.all_tasks]
        tasks.sort(key=lambda t: t.priority, reverse=True)
        for task in tasks:
            print('{} {}'.format(task.name, task.creation_date))
        logging.info('| Tacker show all tasks')

    def _do(self):
        '''Main method thats call other methods
        based on arguments
        '''

        parser = TackerParser.get_parser()
        args = vars(parser.parse_args())
        cr_user = TackerParser.user_parser_choose_dest
        if cr_user in args and args[cr_user] is not None:
            self.choose_user(args[cr_user])
        if self.system.current_user is None:
            print('Use user -c command')
            return
        os.rename(os.path.join(self.path, self.d_name),
                  os.path.join(self.path, self.m_name))
        try:
            if len(sys.argv) <= 1:
                self.print_info()
            for arg_name in self.parser_map:
                if arg_name in args:
                    arg_is_bool = type(args[arg_name]) is bool
                    if args[arg_name]:
                        if arg_is_bool:
                            self.parser_map[arg_name]()
                        else:
                            self.parser_map[arg_name](args[arg_name])
            self.save_data()
        except Exception as e:
            print('{}: {}'.format(type(e).__name__, e))
            logging.error('%s apeared (%s)', type(e).__name__, e)
            self.error = True
        finally:
            os.rename(os.path.join(self.path, self.m_name),
                      os.path.join(self.path, self.d_name))
            if self.error:
                return self.error
            if args[TackerParser.kill_dest]:
                if os.path.exists('tacker.pid'):
                    with open('tacker.pid') as pidfile:
                        pid = int(pidfile.read())
                        os.kill(pid, signal.SIGTERM)
            else:
                background = TackerDaemon(os.getcwd())
                background._do()


def main():
    tacker = Tacker()
    if tacker._do():
        sys.exit(1)


if __name__ == '__main__':
    main()
