import re


def _choices_print(choices):
    if len(choices) == 0:
        return
    if len(choices) == 1:
        return choices[0]
    if len(choices) > 1:
        indexes = [i for i in range(len(choices))]
        for index in indexes:
            name = choices[index].name
            date = choices[index].creation_date
            print('{}. {} {}'.format(index + 1, name, date))
        try:
            index = int(input('Choose (number): '))
        except ValueError:
            print('Wrong input')
            return
        if index - 1 not in indexes:
            print('Wrong input')
        else:
            return choices[index - 1]


def _parse_plan_delta(delta_str):
    if type(delta_str) is not str:
        raise ValueError('delta must be str')
        
    minute = 0
    hour = 0
    day = 0
    month = 0
    year = 0
    params = delta_str.split(' ')
    for param in params:
        result = re.match(r'([0-9]+[mHhDdMYy])', param)
        if result is not None:
            str_res = result.group(0)
            if str_res[-1] == 'm':
                minute += int(str_res[:-1])
            if str_res[-1].lower() == 'h':
                hour += int(str_res[:-1])
            if str_res[-1].lower() == 'd':
                 day += int(str_res[:-1])
            if str_res[-1] == 'M':
                month += int(str_res[:-1])
            if str_res[-1].lower() == 'y':
                year += int(str_res[:-1])
    return year, month, day, hour, minute


def _parse_priority(priority):
    if type(priority) is not str:
        raise ValueError('priority must be str')
        
    result = 0
    result -= priority.count('-')
    result += priority.count('+')
    return result
