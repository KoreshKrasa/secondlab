import unittest
import os
from tacker.tracker import Tacker
from tacker.core.system import System
from tacker.core.task import Status


class TestTacker(unittest.TestCase):

    def setUp(self):
        self.tobj = Tacker()
        self.tobj.system = System()
        self.tobj.choose_user('name')

    def tearDown(self):
        path = os.path.join(os.getcwd(), 'data', 'tackerdata.json')
        open(path, 'w').close()

    @classmethod
    def tearDownClass(cls):
        path = os.path.join(os.getcwd(), 'data', 'tackerdata.json')
        if os.path.exists('config.ini'):
            os.remove('config.ini')
        if os.path.exists('logging.log'):
            os.remove('logging.log')
        if os.path.exists(path):
            os.remove(path)
            os.rmdir('data')


class TestUser(TestTacker):

    def test_user_choose(self):
        self.assertEqual(self.tobj.system.all_users['name'].name,
                         'name')
        self.assertEqual(self.tobj.system.all_users['name'].user_index,
                         self.tobj.system.users_max_index - 1)
        self.assertEqual(self.tobj.system.all_users['name'],
                         self.tobj.system.current_user)

    def test_delete_user(self):
        before = len(self.tobj.system.all_users)
        self.tobj.delete_user()
        after = len(self.tobj.system.all_users)
        self.assertIsNone(self.tobj.system.current_user)
        self.assertEqual(before - 1, after)


class TestList(TestTacker):

    def setUp(self):
        super().setUp()
        self.tobj.create_list('name')
        return self.tobj, 'name', 'name'

    def test_list_choose(self):
        self.tobj.system.current_list = None
        self.tobj.choose_list('name')
        self.assertEqual(self.tobj.system.current_list.name, 'name')

    def test_list_create(self):
        before = len(self.tobj.system.all_users['name'].lists.user_lists)
        self.tobj.create_list('name')
        after = len(self.tobj.system.all_users['name'].lists.user_lists)
        self.assertEqual(before + 1, after)
        self.assertIsNotNone(self.tobj.system.current_list)

    def test_list_rename(self):
        self.tobj.rename_list('newname')
        self.assertEqual(self.tobj.system.current_list.name, 'newname')

    def test_move_list(self):
        parent = self.tobj.system.current_list.parent
        self.tobj.move_list('tlist')
        if self.tobj.system.current_user.lists.find_list('tlist'):
            self.assertEqual(
                self.tobj.system.current_list.parent.name, 'tlist')
        else:
            self.assertIs(self.tobj.system.current_list.parent, parent)

    def test_list_move_task(self):
        cr_list = self.tobj.system.current_list
        self.tobj.system.current_list = None
        self.tobj.add_task('name')
        self.tobj.system.current_list = cr_list
        not_in = self.tobj.system.current_task.index not in self.tobj.system.current_list.tasks
        self.assertEqual(not_in, True)
        self.tobj.move_task_to_list('name')
        in_list = self.tobj.system.current_task.index in self.tobj.system.current_list.tasks
        self.assertEqual(in_list, True)

    def test_list_delete_task(self):
        self.tobj.add_task('name')
        in_list = self.tobj.system.current_task.index in self.tobj.system.current_list.tasks
        self.assertEqual(in_list, True)
        self.tobj.delete_task_from_list('name')
        not_in = self.tobj.system.current_task.index not in self.tobj.system.current_list.tasks
        self.assertEqual(not_in, True)

    def test_list_delete(self):
        before = len(self.tobj.system.current_user.lists.user_lists)
        self.tobj.delete_list()
        after = len(self.tobj.system.current_user.lists.user_lists)
        self.assertEqual(before - 1, after)
        self.assertIsNone(self.tobj.system.current_list)


class TestTask(TestTacker):

    def setUp(self):
        super().setUp()
        self.tobj.add_task('name')

    def test_task_create(self):
        before = len(self.tobj.system.all_tasks)
        self.tobj.add_task('name')
        after = len(self.tobj.system.all_tasks)
        self.assertEqual(before + 1, after)
        self.assertEqual(self.tobj.system.all_tasks[str(
            self.tobj.system.tasks_max_index-1)].name, 'name')

    def test_set_priority(self):
        self.tobj.set_priority('+++-')
        self.assertEqual(self.tobj.system.current_task.priority, 2)

    def test_set_expire_date_ok(self):
        self.tobj.edit_expire_date('12 8 1999 10:30')
        self.assertIsNotNone(self.tobj.system.current_task.expire_date)

    def test_set_expire_date_not_ok(self):
        with self.assertRaises(ValueError):
            self.tobj.edit_expire_date('dfa')

    def test_task_edit_name(self):
        self.tobj.edit_task_name('newname')
        self.assertEqual(self.tobj.system.current_task.name, 'newname')

    def test_task_edit_description(self):
        self.tobj.edit_task_description('description')
        self.assertEqual(
            self.tobj.system.current_task.description, 'description')

    def test_add_subtask(self):
        self.tobj.add_subtask('sub')
        self.assertEqual(len(self.tobj.system.current_task.subtasks), 1)

    def test_add_link(self):
        self.tobj.add_task('link')
        self.tobj.add_link('name')
        self.assertEqual(len(self.tobj.system.current_task.linked), 1)

    def test_reset_task(self):
        self.tobj.reset_task()
        self.assertEqual(self.tobj.system.current_task.status, Status.pending)
        not_in_bin = self.tobj.system.current_task.index not in self.tobj.system.current_user.lists.bin
        self.assertEqual(not_in_bin, True)

    def test_complete_task(self):
        self.tobj.complete_task()
        self.assertEqual(self.tobj.system.current_task.status, Status.done)
        in_bin = self.tobj.system.current_task.index in self.tobj.system.current_user.lists.bin
        self.assertEqual(in_bin, True)

    def test_delete_task(self):
        index = self.tobj.system.current_task.index
        self.tobj.delete_task()
        self.assertEqual(str(index) not in self.tobj.system.all_tasks, True)
        self.assertIsNone(self.tobj.system.current_task)

    def test_set_plan_without_expire(self):
        with self.assertRaises(ValueError):
            self.tobj.set_plan('1h')

    def test_set_plan_with_expire(self):
        self.tobj.edit_expire_date('12 8 1999 10:30')
        self.tobj.set_plan('1h')
        self.assertEqual(len(self.tobj.system.plans), 1)

    def test_delete_plan(self):
        self.tobj.edit_expire_date('12 8 1999 10:30')
        self.tobj.set_plan('1h')
        self.assertEqual(len(self.tobj.system.plans), 1)
        self.tobj.delete_plan()
        self.assertEqual(len(self.tobj.system.plans), 0)

    def test_share_ok(self):
        self.tobj.choose_user('s name')
        self.tobj.choose_user('name')
        self.tobj.choose_task('name')
        self.tobj.share_task('s name')
        self.assertEqual(len(self.tobj.system.current_task.users), 2)

    def test_share_not_ok(self):
        self.tobj.share_task('s name')
        self.assertEqual(len(self.tobj.system.current_task.users), 1)
