import os
import subprocess
import time
from configparser import ConfigParser
from datetime import datetime

import daemon
import daemon.pidfile
from dateutil.relativedelta import relativedelta

import tacker.core.system
import tacker.core.task
import tacker.core.user


class TackerDaemon:

    def __init__(self, cr_dir):
        self.contex = daemon.DaemonContext(
            pidfile=daemon.pidfile.PIDLockFile(os.path.join(cr_dir, 'tacker.pid')))
        self.config = ConfigParser()
        self.dir = cr_dir

    def _do(self):
        with self.contex:
            while True:
                self._repet()
                time.sleep(5)

    def _repet(self):

        path = os.path.join(self.dir, 'config.ini')
        if not os.path.exists(path):
            return
        else:
            self.config.read(path)

        try:
            data_path = self.config['TACKER']['data_path']
            data_name = self.config['TACKER']['data_file_name']
            main_name = self.config['TACKER']['data_file_main'] + data_name
        except KeyError:
            return

        if os.path.isabs(data_path):
            path = data_path
        else:
            path = os.path.join(self.dir, data_path)

        if os.path.exists(os.path.join(path, main_name)):
            return

        tacker_obj = tacker.core.system.System._load_system_data(
            os.path.join(path, data_name))
        enter_time = os.path.getmtime(os.path.join(path, data_name))

        for plan in tacker_obj.plans:
            if plan.task.expire_date is not None:
                while datetime.now() > plan.task.expire_date:
                    # Check date and create new tasks from plan template
                    rd = relativedelta(years=plan.delta.year,
                                       months=plan.delta.month,
                                       days=plan.delta.day,
                                       hours=plan.delta.hour,
                                       minutes=plan.delta.minute)

                    self._set_expire(plan.task, rd)

                    # set available indexes to new tasks
                    tacker_obj.tasks_max_index = self._update_index(
                        plan.task, tacker_obj.tasks_max_index)

                    # add new tasks to all_tasks dict
                    self._to_dict(plan.task, tacker_obj.all_tasks)

                    # give access to users
                    indexes = plan.task._get_sub_indexes()
                    for index in indexes:
                        for user in plan.users:
                            tacker_obj.all_users[user].lists.all_tasks.append(
                                index)

                    # update
                    plan.task = tacker_obj.all_tasks[str(
                        plan.task.index)].copy()

        # update status
        for task in tacker_obj.all_tasks.values():
            if task.expire_date is not None:
                time_is_over = datetime.now() > task.expire_date
                not_done = task.status != tacker.core.task.Status.done
                if time_is_over and not_done:
                    task.status = tacker.core.task.Status.failed

        name = 'temp' + data_name
        tacker_obj._save_system_data(os.path.join(path, name))

        # check is main program is running
        exit_time = os.path.getmtime(os.path.join(path, data_name))
        if exit_time > enter_time:
            return
        if os.path.exists(os.path.join(path, main_name)):
            return

        os.rename(os.path.join(path, name), os.path.join(path, data_name))

    def _set_expire(self, task, dt):
        if type(task) is not tacker.core.task.Task:
            raise TypeError('task must be of Task type')
        if type(dt) is not relativedelta:
            raise TypeError('dt must be of relativedelta type')

        for sub in task.subtasks:
            self._set_expire(sub, dt)
        task.expire_date += dt

    def _update_index(self, task, index):
        if type(task) is not tacker.core.task.Task:
            raise TypeError('task must be of Task type')
        if type(index) is not int:
            raise TypeError('index must be integer number')

        for sub in task.subtasks:
            index = self._update_index(sub, index)
        task.index = index
        index += 1
        return index

    def _to_dict(self, task, target_dict):
        if type(target_dict) is not dict:
            raise TypeError('target_dict is not a dict')
        if type(task) is not tacker.core.task.Task:
            raise TypeError('task must be of Task type')

        for sub in task.subtasks:
            self._to_dict(sub, target_dict)
        target_dict[str(task.index)] = task
