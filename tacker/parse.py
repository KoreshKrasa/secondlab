import argparse


class TackerParser():
    '''Class for argument parser'''
    kill_dest = 'kill'

    # user parser names
    user_parser = 'user'
    user_parser_choose_dest = 'user_choose'
    user_parser_delete_dest = 'user_delete'
    user_parser_show_dest = 'user_show'

    # task parser names
    task_parser_create_dest = 'task_create'
    task_parser_name_dest = 'task_name'
    task_parser_description_dest = 'task_description'
    task_parser_complete_dest = 'task_done'
    task_parser_choose_dest = 'task_choose'
    task_parser_sub_dest = 'task_sub'
    task_parser_link_dest = 'task_link'
    task_parser_delete_dest = 'task_delete'
    task_parser_show_dest = 'task_show'
    task_parser_all_dest = 'task_all'
    task_parser_archive_dest = 'task_archive'
    task_parser_reset_dest = 'task_reset'
    task_parser_time_dest = 'task_time'
    task_parser_plan_dest = 'task_plan'
    task_parser_plan_delete_dest = 'task_plan_delete'
    task_parser_share_dest = 'task_share'
    task_parser_priority_dest = 'task_priority'

    # list parsers names
    list_parser_create_dest = 'list_create'
    list_parser_move_dest = 'list_move'
    list_parser_show_dest = 'list_show'
    list_parser_delete_dest = 'list_demlete'
    list_parser_choose_dest = 'list_choose'
    list_parser_name_dest = 'list_name'
    list_parser_delete_task_dest = 'list_dt'
    list_parser_move_task_dest = 'list_mt'
    list_parser_all_dest = 'list_all'

    @staticmethod
    def get_parser():
        '''This method create and return argument parser

        :return: argument parser
        :rtype: ArgumentParser
        '''
        parser = argparse.ArgumentParser()
        parser.add_argument(
            '-k', '--kill',
            dest=TackerParser.kill_dest,
            action='store_true',
            help='Kill background process')

        subparsers = parser.add_subparsers()

        user_parser = subparsers.add_parser(
            'user', help='Choose what to do with current user')

        user_parser.add_argument(
            '-c', '--choose',
            dest=TackerParser.user_parser_choose_dest,
            help='Choose current user by name')
        user_parser.add_argument(
            '-sw', '--show',
            dest=TackerParser.user_parser_show_dest,
            action='store_true',
            help='Show all users')
        user_parser.add_argument(
            '-d', '--delete',
            dest=TackerParser.user_parser_delete_dest,
            action='store_true',
            help='Delete current user')

        list_parser = subparsers.add_parser(
            'list',
            help='Choose what to do with current user lists')

        list_parser.add_argument(
            '-c',
            '--create',
            dest=TackerParser.list_parser_create_dest,
            help='Create new list')
        list_parser.add_argument(
            '-m', '--move',
            dest=TackerParser.list_parser_move_dest,
            help='Move curent list')
        list_parser.add_argument(
            '-sh', '--show',
            dest=TackerParser.list_parser_show_dest,
            action='store_true',
            help='Show current list')
        list_parser.add_argument(
            '-d', '--delete',
            dest=TackerParser.list_parser_delete_dest,
            action='store_true',
            help='Delete current list')
        list_parser.add_argument(
            '-ch', '--choose',
            dest=TackerParser.list_parser_choose_dest,
            help='Choose current list')
        list_parser.add_argument(
            '-n', '--name',
            dest=TackerParser.list_parser_name_dest,
            help='Rename current list')
        list_parser.add_argument(
            '-dt', '--delete_task',
            dest=TackerParser.list_parser_delete_task_dest,
            help='Remove task from current list')
        list_parser.add_argument(
            '-mt', '--move_task',
            dest=TackerParser.list_parser_move_task_dest,
            help='Move task to current list')
        list_parser.add_argument(
            '-a', '--all',
            dest=TackerParser.list_parser_all_dest,
            action='store_true',
            help='Show all lists')

        task_parser = subparsers.add_parser(
            'task',
            help='Choose what to do with task')

        task_parser.add_argument(
            '-c', '--create',
            dest=TackerParser.task_parser_create_dest,
            help='Create new task')
        task_parser.add_argument(
            '-n', '--name',
            dest=TackerParser.task_parser_name_dest,
            help='Edit task name')
        task_parser.add_argument(
            '-de', '--description',
            dest=TackerParser.task_parser_description_dest,
            help='Edit task description')
        task_parser.add_argument(
            '-dn', '--done',
            dest=TackerParser.task_parser_complete_dest,
            action='store_true',
            help='Complete current task')
        task_parser.add_argument(
            '-ch', '--choose',
            dest=TackerParser.task_parser_choose_dest,
            help='Choose current task'
        )
        task_parser.add_argument(
            '-s', '--subtask',
            dest=TackerParser.task_parser_sub_dest,
            help='Add to current task sub-task')
        task_parser.add_argument(
            '-l', '--link',
            dest=TackerParser.task_parser_link_dest,
            help='Add to current task linked task')
        task_parser.add_argument(
            '-sw', '--show',
            dest=TackerParser.task_parser_show_dest,
            action='store_true',
            help='Show current task')
        task_parser.add_argument(
            '-dt', '--delete',
            dest=TackerParser.task_parser_delete_dest,
            action='store_true',
            help='Delete current task')
        task_parser.add_argument(
            '-a', '--all',
            dest=TackerParser.task_parser_all_dest,
            action='store_true',
            help='Show all tasks')
        task_parser.add_argument(
            '-ar', '--archive',
            dest=TackerParser.task_parser_archive_dest,
            action='store_true',
            help='Show completed tasks')
        task_parser.add_argument(
            '-tl', '--time_limit',
            dest=TackerParser.task_parser_time_dest,
            help='Set expire date')
        task_parser.add_argument(
            '-p', '--plan',
            dest=TackerParser.task_parser_plan_dest,
            help='Create plan')
        task_parser.add_argument(
            '-dp', '--delete_plan',
            dest=TackerParser.task_parser_plan_delete_dest,
            action='store_true',
            help='Delete plan')
        task_parser.add_argument(
            '-sh', '--share',
            dest=TackerParser.task_parser_share_dest,
            help='Share task')
        task_parser.add_argument(
            '-pr', '--priority',
            dest=TackerParser.task_parser_priority_dest,
            help='Set task priority')
        task_parser.add_argument(
            '-r', '--reset',
            dest=TackerParser.task_parser_reset_dest,
            action='store_true',
            help='Reset task')

        return parser
