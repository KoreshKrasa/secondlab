import os
import jsonpickle
import json


class System:
    '''Class for holding tracker data'''
    
    def __init__(self):
        self.all_users = {}
        self.all_tasks = {}
        self.plans = []
        self.users_max_index = 1
        self.tasks_max_index = 1
        self.current_user = None
        self.current_list = None
        self.current_task = None

    @staticmethod
    def _load_system_data(path):
        if os.path.exists(path):
            with open(path, 'r') as file:
                try:
                    system_obj = jsonpickle.decode(file.read())
                except json.decoder.JSONDecodeError:
                    system_obj = System()
                finally:
                    return system_obj

    def _save_system_data(self, path):
        json_data = json.dumps(json.loads(jsonpickle.encode(self)), indent=4)
        with open(path, 'w') as file:
            file.write(json_data)
