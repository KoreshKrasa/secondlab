from datetime import datetime
import json


class User:
    '''Class for user representation'''

    def __init__(self, user_index, user_name=None):

        if type(user_index) is not int:
            raise ValueError('index must be int')

        if user_name is None:
            user_name = 'New user'

        self.user_index = user_index
        self.name = user_name
        self.lists = Lists()

    def create_list(self, list_name=None, parent_list=None):
        '''Create new list for user

        :param list_name: Name of the list
        :param parent_list: List which will hold new list
        :type list_name: list_name must be str type
        :type parent_list: parent_list must be List type
        :return: This method create created list
        :rtype: Return List type object
        '''

        new_list = List(list_name, parent_list)
        self.lists.user_lists.append(new_list)
        return new_list

    def delete_list(self, tlist=None):
        '''Delete list

        :param tlist: List to delete
        :type tlist: tlist must be List type
        :raise ValueError: If tlist is not List
        '''

        if tlist is None:
            raise ValueError('tlist must be of List type')

        if tlist in self.lists.user_lists:
            if tlist.parent is not None:
                tlist.parent.childs.remove(tlist)
            for child in tlist.childs:
                self.delete_list(child)
            self.lists.user_lists.remove(tlist)

    def move_list_to_list(self, tlist=None, parent=None):
        '''Move list to another list

        :param tlist: List to be moved
        :param parent:  New parent list for tlist
        :type tlist: tlsit must be List type
        :type parent: parent may be List type or None
        :raise ValueError: If tlist is not List
        :raise ValueError: If tlist is not List or None
        '''

        if tlist is None:
            raise ValueError('tlist must be of List type')

        if type(parent) is not List and parent is not None:
            raise ValueError('parent must be List or None')

        if tlist.parent is not None:
            tlist.parent.childs.remove(tlist)
        tlist.parent = parent
        if parent is not None:
            parent.childs.append(tlist)

    def move_task_to_list(self, index, tlist):
        '''Add task to list

        :param index: Index of a task to add
        :param tlist: List for task
        :type index: Must be int
        :type tlist: Must be List
        :raise ValueError: If tlist is not List
        :raise ValueError: If index is not int
        '''

        if tlist is None:
            raise ValueError('tlist must be List type')

        if type(index) is not int:
            raise ValueError('index must be int')

        if index in self.lists.all_tasks:
            tlist.tasks.append(index)

    def remчove_task_from_list(self, index, tlist):
        '''Remove task from list

        :param index: Index of a task to remove
        :param tlist: List with task
        :type index: Must be int
        :type tlist: Must be List
        :raise ValueError: If tlist is not List
        :raise ValueError: If index is not int
        '''

        if tlist is None:
            raise ValueError('tlist must be List type')

        if type(index) is not int:
            raise ValueError('index must be int')

        if index in self.lists.all_tasks:
            if index in tlist:
                tlist.tasks.remove(index)

    def add_task(self, task_index):
        '''Add task index to user

        :param  task_index: Index of the task
        :type task_index: Must be int
        :raise ValueError: If index is not int
        '''

        if type(task_index) is not int:
            raise ValueError('index must be int')

        self.lists.all_tasks.append(task_index)


class List:
    '''Class for list reprezentation'''

    def __init__(self, name=None, parent=None):
        if name is None:
            name = 'New list'
        self.name = name
        self.parent = parent
        self.childs = []
        self.tasks = []
        self.creation_date = datetime.now()


class Lists:
    '''Class to hold user lists and some defoult one'''

    def __init__(self):
        self.all_tasks = []
        self.bin = []
        self.user_lists = []

    def find_list(self, list_name):
        '''Search list with specific name

        :param list_name: Name of the list to be found
        :type list_name: Must be str type
        '''

        lists_to_return = []
        for list in self.user_lists:
            if list.name == list_name:
                lists_to_return.append(list)
        return lists_to_return
