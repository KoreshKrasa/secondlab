import json
from datetime import datetime


class Task:
    '''Class for task representation'''

    def __init__(self, index=None, name=None):
        if type(index) is not int:
            raise ValueError('index must be int')

        if name is None:
            name = 'New task'

        self.name = name
        self.description = None
        self.index = index
        self.status = Status.pending
        self.parent = None

        self.users = []
        self.subtasks = []
        self.linked = []
        self.linked_parent = []

        self.creation_date = datetime.now()
        self.expire_date = None
        self.complete_date = None
        self.priority = 0

    def complete_task(self):
        '''This method change task properties to complete form

        :return: Result of completion True if successful/False if not
        :rtype: Bool 
        '''

        for linked_task in self.linked:
            if linked_task.status != Status.done:
                return False
        for subtask in self.subtasks:
            subtask.complete_task()
        self.status = Status.done
        self.complete_date = datetime.now()
        return True

    # get indexes from task tree
    def _get_sub_indexes(self, indexes=None):
        if indexes is None:
            indexes = list()
        for sub in self.subtasks:
            sub._get_sub_indexes(indexes)
        indexes.append(self.index)
        return indexes

    def reset(self):
        ''' Return task state to pending'''

        if self.status != Status.pending:
            self.status = Status.pending
            self.complete_date = None
            for sub_task in self.subtasks:
                sub_task.reset()

    # create tamplate for planing
    def copy(self):
        '''Create copy of a task

        :return: Copy of a task
        :rtype: Task object
        '''

        copyed_task = Task(self.index, self.name)
        copyed_task.description = self.description
        copyed_task.expire_date = self.expire_date
        copyed_task.users = self.users.copy()
        copyed_task.priority = self.priority
        for sub in self.subtasks:
            copyed_task.subtasks.append(sub.copy())
        return copyed_task


class Plan:
    '''Class for plans representation'''

    def __init__(self, delta, task):
        self.delta = delta
        self.users = []
        self.name = task.name
        self.task = task.copy()
        self.creation_date = datetime.now()

    def add_user(self, name=None):
        if name is None:
            return
        self.users.append(name)


class Delta:
    '''Class to store delta for plans'''

    def __init__(self, year=None, month=None, day=None, hour=None, minute=None):
        self.year = year
        self.month = month
        self.day = day
        self.hour = hour
        self.minute = minute


class Status:
    '''Statuses for task'''

    pending = 'PENDING'
    done = 'DONE'
    failed = 'FAILED'
